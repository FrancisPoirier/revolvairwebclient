import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { PollutantInformationComponent } from './pollutant-information.component';
import { PollutantService } from '../services/pollutant-service/pollutant.service';
import { Observable} from 'rxjs/Observable';
import { Pollutant } from '../models/pollutant';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PollutantInformationComponent', () => {
  let component: PollutantInformationComponent;
  let fixture: ComponentFixture<PollutantInformationComponent>;
  let pollutantService: PollutantService;
  let pollutant: Pollutant;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ PollutantInformationComponent ],
      providers: [ PollutantService ],
      imports: [
        AccordionModule,
        HttpClientTestingModule
      ]
    });
    fixture = TestBed.createComponent(PollutantInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    pollutantService = TestBed.get(PollutantService);
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create pollutantService', () => {
    expect(pollutantService).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should fetch pollutants of the API from the pollutantService', fakeAsync(() => {
        spyOn(pollutantService, 'getPollutants').and.returnValue(Observable.of([pollutant]));
        tick();

        component.ngOnInit();
        tick();

        expect(pollutantService.getPollutants).toHaveBeenCalled();
    }));
  });
});
