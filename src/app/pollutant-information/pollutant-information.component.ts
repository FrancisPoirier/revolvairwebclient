import { Component, OnInit } from '@angular/core';
import { Pollutant } from '../models/pollutant';
import { PollutantService } from '../services/pollutant-service/pollutant.service';

@Component({
  selector: 'app-pollutant-information',
  templateUrl: './pollutant-information.component.html',
  styleUrls: ['./pollutant-information.component.css']
})
export class PollutantInformationComponent implements OnInit {
  pollutants: Pollutant[];

  constructor(private pollutantService: PollutantService) { }

  ngOnInit() {
   this.getPollutants();
  }

  getPollutants(): void {
    this.pollutantService.getPollutants().subscribe(pollutants => this.pollutants = pollutants);
  }
}
