import {async, tick} from '@angular/core/testing';

import { ForgotPasswordComponent } from './forgot-password.component';
import {Observable} from 'rxjs/Observable';

describe('ForgotPasswordComponent', () => {
  let forgotPasswordComponent;
  let userServiceMock;

  let EMAIL = 'test@test.test';

  beforeEach(async(() => {
      userServiceMock = jasmine.createSpyObj('UserService', ['forgotPassword']);
      forgotPasswordComponent = new ForgotPasswordComponent(userServiceMock);
  }));

      it('submitting form calls service\'s forgotPassword() method',() => {
          // Arrange
          userServiceMock.forgotPassword.and.returnValue(Observable.of({'result':'success'}));
          forgotPasswordComponent.model.email = EMAIL;

          // Act
          forgotPasswordComponent.onSubmit();

          // Assert
          expect(userServiceMock.forgotPassword).toHaveBeenCalled();
      });

    it('email not found displays error',() => {
        // Arrange
        userServiceMock.forgotPassword.and.returnValue(Observable.of({'error':'email not found'}));
        forgotPasswordComponent.model.email = EMAIL;

        // Act
        forgotPasswordComponent.onSubmit();

        // Assert
        expect(forgotPasswordComponent.message).toBe('Adresse courriel introuvable');
    });

    it('email found success message',() => {
        // Arrange
        userServiceMock.forgotPassword.and.returnValue(Observable.of({'result':'success'}));
        forgotPasswordComponent.model.email = EMAIL;

        // Act
        forgotPasswordComponent.onSubmit();

        // Assert
        expect(forgotPasswordComponent.message).toBe('Vous allez recevoir un courriel contenant un lien sous peu.');
    });
});
