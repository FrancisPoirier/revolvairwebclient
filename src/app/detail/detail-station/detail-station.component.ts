import {Component, NgModule, OnInit} from '@angular/core';
import {Station} from '../../models/station';
import {StationService} from '../../services/station-service/station.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';

@Component({
  selector: 'detail-station',
  templateUrl: './detail-station.component.html',
  styleUrls: ['./detail-station.component.css'],
})
export class DetailStationComponent implements OnInit {

  constructor(
    private stationService : StationService,
    private route: ActivatedRoute,
    private router : Router
  ) {
    this.route.params.subscribe( params => this.params = params);
  }

  params: Params;
  station: Station;
  aqiData = [];
  private getObjectKeys = ObjectKeyService.getObjectKeys;

  ngOnInit() {
    this.stationService.getStation(this.params["id"]).subscribe(
      station => this.station = station
    );

    this.stationService.getLatestAqi(this.params["id"]).subscribe(
      latestAqi => this.setAqiData(latestAqi)
    );
  }


  back(){
    this.router.navigate(["list-station"]);
  }


  private setAqiData(latestAqi){
    for( let key in latestAqi){
      this.aqiData[key] = (latestAqi[key]);
    }
  }
}
