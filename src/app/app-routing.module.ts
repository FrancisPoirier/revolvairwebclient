import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StationMapComponent} from './station-map/station-map.component';
import {ListStationComponent} from './list/list-station/list-station.component';
import {DetailStationComponent} from './detail/detail-station/detail-station.component';
import {DetailSensorComponent} from './detail/detail-sensor/detail-sensor.component';
import {StationInfoComponent} from './station-info/station-info.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {EmailSubscriptionComponent} from './email/email-subscription/email-subscription.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import { PollutantInformationComponent } from './pollutant-information/pollutant-information.component';

export const routes: Routes = [
    { path: '', component: StationMapComponent, pathMatch: 'full' },
    { path: 'list-station', component: ListStationComponent, pathMatch: 'full' },
    { path: 'station-info/:id', component: StationInfoComponent, pathMatch: 'full' },
    { path: 'detail-station/:id', component: DetailStationComponent, pathMatch: 'full' },
    { path: 'detail-station/:stationId/detail-sensor/:sensorId', component: DetailSensorComponent, pathMatch: 'full' },
    { path: 'sign-up', component: SignUpComponent, pathMatch: 'full' },
    { path: 'subscribe', component: EmailSubscriptionComponent, pathMatch: 'full' },
    { path: 'sign-in', component: SignInComponent, pathMatch: 'full' },
    { path: 'forgot-password', component: ForgotPasswordComponent, pathMatch: 'full' },
    { path: 'resetpassword/:token', component: ResetPasswordComponent, pathMatch: 'full' },
    { path: 'pollutants', component: PollutantInformationComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
