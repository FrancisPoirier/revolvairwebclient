import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Pollutant } from '../../models/pollutant';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class PollutantService {
    apiUrl = 'http://revolvairapi.test/api';

    constructor(private http: HttpClient) { }

    getPollutants(): Observable<Pollutant[]> {
        return this.http.get<Pollutant[]>(`${this.apiUrl}/pollutants`)
            .pipe(
                catchError(this.handleError([])
            ));
    }

    private handleError<T> (result?: T) {
        return (): Observable<T> => {
          return of(result as T);
        };
      }
}
