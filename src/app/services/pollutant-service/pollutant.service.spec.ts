import { TestBed, async, fakeAsync, tick, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { PollutantService } from './pollutant.service';
import { Pollutant } from '../../models/pollutant';
import { Observable } from 'rxjs/Observable';

describe('PollutantService', () => {
    let pollutantService: PollutantService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [ PollutantService ]
        });
        pollutantService = TestBed.get(PollutantService);
        httpMock = TestBed.get(HttpTestingController);
    });
    afterEach(() => {
        httpMock.verify();
    });

    it('should get the pollutants from the API url', fakeAsync(() => {
        const expectedPollutants: Pollutant[] = [];

        pollutantService.getPollutants().subscribe(pollutants => {
            expect(pollutants.length).toBe(expectedPollutants.length);
            expect(pollutants).toEqual(expectedPollutants);
        });
        tick();

        const request = httpMock.expectOne(`${pollutantService.apiUrl}/pollutants`);

        expect(request.request.method).toBe('GET');
        request.flush(expectedPollutants);
    }));

    it('should return empty array if an error occurs while fetching polluants', async(() => {
        // Arrange
        inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
            spyOn(http, 'get').and.returnValue(Observable.throw('An error occured.'));

            // Act
            const actualValue = pollutantService.getPollutants();

            // Assert
            const expectedValue = [];
            expect(actualValue).toEqual(expectedValue);
        });
    }));
});
