import {TestBed, inject, async} from '@angular/core/testing';

import { LoginService } from './login.service';
import { environment } from '../../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('LoginService', () => {
  let loginService: LoginService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [LoginService]
    });
    loginService = TestBed.get(LoginService);
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));

  describe('login()', () => {
    it('should post user info to apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          const expectedUrl = apiUrl + '/login';
          const email = 'ced.toup@hotmail.com';
          const password = 'secret';

          //Act
          loginService.login(email, password).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'POST'
          });
        })));
  });
});
