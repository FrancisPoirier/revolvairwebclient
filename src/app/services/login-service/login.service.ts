import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<Object> {
    const url = `${this.apiUrl}/login`;
    return this.http.post(url, {
      email: email,
      password: password,
    }, {observe: 'body'});
  }
}
