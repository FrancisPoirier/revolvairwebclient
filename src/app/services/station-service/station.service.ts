import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {Station} from '../../models/station';
import {environment} from '../../../environments/environment';

@Injectable()
export class StationService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getStations(): Observable<Station[]> {
    const url = `${this.apiUrl}/stations`;

    return this.http.get<Station[]>(url)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  getStation(id: number): Observable<Station> {
    const url = `${this.apiUrl}/stations/${id}`;

    return this.http.get<Station>(url)
      .pipe(
        catchError(this.handleError(new Station))
      );
  }

  getLatestAqi(id: number): Observable<number[]> {
    const url = `${this.apiUrl}/stations/${id}/sensors/latest-aqi`;

    return this.http.get<number[]>(url)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  private handleError<T> (result?: T) {
    return (): Observable<T> => {
      // Retourner une réponse vide.
      return Observable.of(result as T);
    };
  }
}
